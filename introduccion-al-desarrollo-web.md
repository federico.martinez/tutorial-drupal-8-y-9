![Screenshot](img/introduccion-desarrollo-web.jpg)

Este capítulo está inspirado en la charla "How the Internet works" de [Jessica McKellar](http://web.mit.edu/jesstess/www/){target=_blank}.
Compartiremos un par de conocimientos básicos que necesitamos a la hora de empezar a navegar en el mundo del desarrollo web. ¡Comencemos!

## ¿Cómo funciona internet?
Apostamos a que utilizas Internet todos los días. Pero, ¿sabes lo que pasa cuando escribes una dirección como https://www.drupal.org en tu navegador y presionas enter?

La primera cosa que necesitas entender, es que una página web consiste de un puñado de archivos guardados en el disco duro -- como tus películas, música, o imágenes. Sin embargo, hay una parte que es única para los sitios web: ellos incluyen código computarizado llamado HTML.

Es un lenguaje de marcas de hipertexto para definir elementos como textos, encabezados, imágenes, tablas, links. Es la base de toda página web, de hecho una página web estática y sencilla no es más que una selección de archivos HTML interconectados que sólo muestran información sin permitir interacción por parte del visitante.

```
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
     <link rel="stylesheet" href="styles.css">
		<title>Hola Mundo!</title>
	</head>
	<body>
		<h1>Hola Mundo!</h1>
    <p>Este es mi primer blog!</p>
	</body>
</html>
```

Se linkean con hojas de estilo en cascada o CSS para asignar reglas sobre diseño, color y tipografía, así como animaciones, transiciones de color y sombras.

![Screenshot](img/css_slide.png)

Si no estás familiarizada con la programación, puede ser difícil de comprender HTML a la primera, pero tus navegadores web (como Chrome, Firefox, Safari, etc.) lo aman. Los navegadores están diseñados para entender ese código, seguir sus instrucciones y presentar estos archivos de los cuales está hecho tu sitio web, exactamente de la forma que quieres.

Como cualquier otro archivo, tenemos que guardar los archivos HTML en algún lugar de un disco duro. Para Internet, utilizamos equipos especiales, de gran alcance llamados servidores. Estos no tienen una pantalla, ratón o teclado, debido a que su propósito es almacenar datos y servirlos. Por esa razón son llamados servidores -- porque sirven los datos. Así que, básicamente, cuando tienes un sitio web necesitas tener un servidor (la máquina) donde este vive. Cuando el servidor recibe una petición entrante (una request), éste devuelve su sitio de Internet y la petición puede venir de cualquier lado!

OK, pero cómo se ve Internet, ¿cierto? Internet es una red de máquinas interconectadas (los servidores que nombramos anteriormente). ¡Cientos de miles de máquinas! ¡Muchos, muchos kilómetros de cables alrededor del mundo! Puedes visitar el sitio web [Submarine Cable Map ](http://submarinecablemap.com/){target=_blank} y ver lo complicada que es la red.

![Screenshot](img/red.png)

Entonces, podemos decir que las peticiones van de una computadora a otra hasta llegar al servidor donde se encuentre tu sitio web. <br>
Así es como funciona - se envían peticiones y se espera una respuesta.

## Drupal es más que HTML, es un CMS
Un CMS (del inglés Content Management System) es un programa que permite crear y administrar sitios web que tengan funcionalidades tales como completar un formulario, escribir en un foro, realizar una compra de un producto, etc.
Este tipo de sitios web requiren cierta lógica que se consigue con lenguajes de programación como PHP.

## Introducción a la interfaz de línea de comandos
Permítenos presentarte a tu primera amiga nueva: ¡la línea de comandos!

Los siguientes pasos te mostrarán cómo usar aquella ventana negra que todos los hackers usan. Puede parecer un poco aterrador al principio pero es solo un mensaje en pantalla que espera a que le des órdenes.

## ¿Qué es la línea de comandos?

La ventana, que generalmente es llamada línea de comandos ó interfaz de línea de comandos, es una aplicación basada en texto para ver, manejar y manipular archivos en tu computadora. Te presentamos algunos nombres de la línea de comandos: cmd, CLI, prompt -símbolo de sistema-, consola o terminal.

## Abrir la interfaz de línea de comandos

Para empezar con algunos experimentos necesitarás abrir nuestra interfaz de línea de comandos en primer lugar.

<h5>En GNU/Linux</h5>
Probablemente se encuentre en Aplicaciones → Accesorios → Terminal, o Aplicaciones → Sistema → Terminal, aunque esto dependerá de tu sistema. Si no lo encuentras allí, intenta buscarlo en Google. :)

<h5>En macOS</h5>
Ve a Aplicaciones → Utilidades → Terminal.

<h5>En Windows</h5>
Dependiendo de tu versión de Windows y tu teclado, una de las opciones siguientes debería abrir una ventana de comandos (puede que necesites experimentar un poco, pero no se necesita probar todas estas sugerencias):

* Ve al menú o pantalla de Inicio, y escribe "Símbolo del Sistema" en el cuadro de búsqueda.
* Ve a Menú de inicio → Windows System → Command Prompt.
* Ve al menú de Inicio → Todos los Programas → Accessorios → Símbolo del Sistema.
* Ve a la pantalla de Inicio, pasa el ratón sobre la esquina inferior izquierda de la pantalla, y haz clic en la flecha hacia abajo (en una pantalla táctil, desliza hacia arriba desde la parte baja de la pantalla). La página de la Aplicación debería abrirse. Haz clic en Símbolo del Sistema en la sección Sistema de Windows.
* Mantén la tecla especial de Windows de tu teclado y pulsa "X". Elige "Símbolo del Sistema" del menú emergente.
* Mantén pulsada la tecla de Windows y pulsa "R" para abrir una ventana "Ejecutar". Escribe "cmd" en la caja, y haz clic en OK.

![Screenshot](img/windows.png)

Si quieres practicar comandos desde la consola te invitamos a hacer esta [práctica](https://tutorial.djangogirls.org/es/intro_to_command_line/){target=_blank}

Desde la línea de comandos estaremos usando un programa llamado GIT.

## ¿Que es GIT?
Git es un programa de control de versiones que puede seguir los cambios realizados en archivos a lo largo del tiempo de forma que más tarde puedas volver a cualquier versión anterior. Algo similar a la herramienta de "Control de Cambios" en los programas de tipo Word (por ejemplo, Microsoft Word o LibreOffice Writer), pero mucho más potente.

<h5>Necesitaremos tener instalado GIT en neustra computadora para subir nuestro proyecto al servidor, que lo publicará en internet.</h5>

Te dejamos un video para que aprendas más sobre [GIT](https://www.youtube.com/watch?v=zH3I1DZNovk){target=_blank}

![Screenshot](img/git-muestra.png)

## Cómo instalar GIT

<h5>En Windows</h5>
Puedes descargar Git desde git-scm.com. Puedes hacer click en "Next" en todos los pasos excepto en dos: cuando se te pregunte que selecciones tu editor, selecciona Nano, y en el paso "adjusting your PATH environment", selecciona "Use Git and optional Unix tools from the Windows Command Prompt" (la última opción). Aparte de eso, los valores por defecto son correctos. "Checkout Windows-style, commit Unix-style line endings" tampoco necesita corrección.
No olvides reiniciar el Símbolo del Sistema o el PowerShell una vez que la instalación se complete con éxito.

<h5>En Linux Debian/Ubuntu</h5>
Desde la linea de comandos
```
sudo apt install git
```
<h5>En MacOS</h5>

Hay muchas formas de instalar GIT en un equipo con Mac, incluso puede que ya esté instalado si tienes el XCode instalado. Podés ejecutar el siguiente comando en la terminal para chequearlo:
```
git --version
```

<br>
## También necesitaremos un editor de texto o una interfaz gráfica de desarrollo. ¿Por qué?

Te podrías estar preguntando por qué estamos instalando un editor de código en vez de usar algo como Word o Notepad.

La primera razón es que el código necesita ser texto plano, y el problema con programas como Word o similares es que no producen texto plano, sino texto enriquecido (con fuentes y formatos).

La segunda razón es que los editores de código pueden proveer ayuda como destacar código con color acorde a su significado, o automáticamente completar código o corregirlo.

Pronto pensarás en convertir el editor de código en una de tus herramientas favoritas. :)

<h5>Atom</h5>
Atom es un editor muy popular. Es gratis, de código abierto y disponible para Windows, OS X and Linux.
Lo podés descargar de [aquí](https://atom.io/){target=_blank}

<h5>Vim</h5>
Vim es otro editor de texto y es el que usaremos en este tutorial.
Lo podés descargar de [aquí](https://www.vim.org/download.php){target=_blank}

Suficiente conversación - ¡tiempo de crear!
