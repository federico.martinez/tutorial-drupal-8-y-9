![Screenshot](img/drupal-welcome.jpg)

## Comenzando

<h3>¡Nos alegra que estés aquí :)</h3>
En este tutorial, te llevaremos de viaje a las entrañas de Drupal 8 para que puedas ver todas las piezas que se necesitan para que un sitio web funcione.

¿Alguna vez te has planteado crear un sitio web pero no sabías por dónde empezar? ¿Has pensado alguna vez que el mundo del software es demasiado complicado como para intentar hacer algo por tu cuenta?

¡Tenemos buenas noticias! Crear un sitio web con Drupal 8, no es tan difícil como parece y aqui te demostraremos lo divertido que puede ser.

¡Esperamos conseguir que te guste el desarrollo web tanto como a nosotras!

Te invitamos a visitar la web oficial de [Drupal](https://www.drupal.org){target=_blank} y la [Guía de Drupal 8](https://www.drupal.org/es/docs/user_guide/es/index.html){target=_blank}.

## ¿Qué aprenderás en este tutorial?

Cuando termines el tutorial, tendrás una aplicación web sencilla y funcional: tu propio blog.
Aprenderemos cómo recibir mensajes desde tu sitio Drupal a través de WhatsApp y cómo publicar tu web en internet para que todas las personas puedan acceder y ver tu sitio.


## Sobre nosotras y cómo contribuir

Participamos de la comunidad de [Drupal en Buenos Aires](http://www.drupalba.com.ar){target=_blank} y [Buenos Aires Drupal Meetup](https://www.meetup.com/es/Buenos-Aires-Drupal-Meetup){target=_blank}.
Podés sumarte a nuestro canal [Meetup Drupal BA](https://www.meetup.com/es/Buenos-Aires-Drupal-Meetup){target=_blank}, sin importar en que punto geográfico te encuentres, o escribirnos si encontrás algún error o si te interesa actualizar o mejorar el tutorial a comunidad@drupalba.com.ar o via Twitter a [DrupalBA](https://twitter.com/drupalba){target=_blank}.

## Licencia

Este trabajo está bajo la licencia [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/){target=_blank}, agradecemos a [Django Girls](http://djangogirls.org){target=_blank} por inspirar este tutorial y recomendamos muchícimo aprender Django con su [guía](https://tutorial.djangogirls.org/){target=_blank}.
